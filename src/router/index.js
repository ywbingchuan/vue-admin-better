/**
 * @author chuzhixin 1204505056@qq.com （不想保留author可删除）
 * @description router全局配置，如有必要可分文件抽离，其中asyncRoutes只有在intelligence模式下才会用到，vip文档中已提供路由的基础图标与小清新图标的配置方案，请仔细阅读
 */

import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '@/layouts'
import EmptyLayout from '@/layouts/EmptyLayout'
import { publicPath, routerMode } from '@/config'

Vue.use(VueRouter)
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true,
  },
  {
    path: '/register',
    component: () => import('@/views/register/index'),
    hidden: true,
  },
  {
    path: '/401',
    name: '401',
    component: () => import('@/views/401'),
    hidden: true,
  },
  {
    path: '/404',
    name: '404',
    component: () => import('@/views/404'),
    hidden: true,
  },
]

export const asyncRoutes = [
  {
    path: '/',
    component: Layout,
    redirect: '/index',
    children: [
      {
        path: 'index',
        name: 'Index',
        component: () => import('@/views/index/index'),
        meta: {
          title: '首页',
          icon: 'home',
          affix: true,
        },
      },
    ],
  },
  {
    path: "/system",
    component: Layout,
    redirect: "noRedirect",
    name: 'System',
    alwaysShow: true,
    meta: { title: '系统设置', icon: 'box-open' },
    children: [
      {
        path: "admin",
        name: "Admin",
        component: () => import("@/views/system/admin/index"),
        meta: {
          title: "系统用户",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "resetpass",
        name: "ResetPass",
        component: () => import("@/views/system/resetPass/index"),
        meta: {
          title: "重置密码",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "siteConfig",
        name: "SiteConfig",
        component: () => import("@/views/system/siteConfig/index"),
        meta: {
          title: "网站设置",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "upload",
        name: "UploadConfig",
        component: () => import("@/views/system/uploadConfig/index"),
        meta: {
          title: "上传设置",
          icon: "marker",
          permissions: ["admin"],
        },
      },
    ],
  },
  {
    path: "/news",
    component: Layout,
    redirect: "noRedirect",
    name: 'News',
    alwaysShow: true,
    meta: { title: '新闻系统', icon: 'box-open' },
    children: [
      {
        path: "authorGroup",
        name: "AuthorGroup",
        component: () => import("@/views/news/authorGroup/index"),
        meta: {
          title: "作者分组",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "author",
        name: "Author",
        component: () => import("@/views/news/author/index"),
        meta: {
          title: "作者管理",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "type",
        name: "ArticleType",
        component: () => import("@/views/news/type/index"),
        meta: {
          title: "栏目管理",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "meet",
        name: "ArticleMeet",
        component: () => import("@/views/news/meet/index"),
        meta: {
          title: "近期活动",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "meetAdd/:meetId",
        name: "ArticleMeetAdd",
        component: () => import("@/views/news/meet/add"),
        hidden: true,
        props: true,
        meta: {
          title: "编辑活动",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "carousel",
        name: "Carousel",
        component: () => import("@/views/news/carousel/index"),
        meta: {
          title: "Banner管理",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "carouselAdd/:carouselId",
        name: "CarouselAdd",
        component: () => import("@/views/news/carousel/add"),
        hidden: true,
        props: true,
        meta: {
          title: "Banner编辑",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "vars/:typeId",
        name: "Vars",
        component: () => import("@/views/news/vars/index"),
        hidden: true,
        props: true,
        meta: {
          title: "栏目配置",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "list/:typeId",
        name: "List",
        component: () => import("@/views/news/list/index"),
        hidden: true,
        props: true,
        meta: {
          title: "文章管理",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "article/:typeId/:articleId",
        name: "Article",
        component: () => import("@/views/news/article/index"),
        hidden: true,
        props: true,
        meta: {
          title: "编辑文章",
          icon: "marker",
          permissions: ["admin"],
        },
      },
    ],
  },
  {
    path: "/resource",
    component: Layout,
    redirect: "noRedirect",
    name: 'Resource',
    alwaysShow: true,
    meta: { title: '资源管理', icon: 'box-open' },
    children: [
      {
        path: "users",
        name: "Users",
        component: () => import("@/views/resource/users/index"),
        meta: {
          title: "用户管理",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "image",
        name: "ImageRes",
        component: () => import("@/views/resource/resimage/index"),
        meta: {
          title: "图片管理",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "video",
        name: "VideoRes",
        component: () => import("@/views/resource/resvideo/index"),
        meta: {
          title: "视频管理",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "docs",
        name: "UploadDocs",
        component: () => import("@/views/resource/resdocs/index"),
        meta: {
          title: "文档管理",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "report",
        name: "ReportDocs",
        component: () => import("@/views/resource/reportdocs/index"),
        meta: {
          title: "报告管理",
          icon: "marker",
          permissions: ["admin"],
        },
      },
    ],
  },
  {
    path: "/form",
    component: Layout,
    redirect: "noRedirect",
    name: 'FormData',
    alwaysShow: true,
    meta: { title: '表单数据', icon: 'box-open' },
    children: [
      {
        path: "buy",
        name: "FormBuy",
        component: () => import("@/views/resource/buy/index"),
        meta: {
          title: "购买咨询",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "ask",
        name: "FormAsk",
        component: () => import("@/views/resource/ask/index"),
        meta: {
          title: "问题咨询",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "agree",
        name: "FormAgree",
        component: () => import("@/views/resource/agree/index"),
        meta: {
          title: "合作意向",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "apply",
        name: "FormApply",
        component: () => import("@/views/resource/apply/index"),
        meta: {
          title: "申请参赛",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "company",
        name: "BoaoCompany",
        component: () => import("@/views/resource/company/index"),
        meta: {
          title: "博奥奖-企业",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "person",
        name: "BoaoPersons",
        component: () => import("@/views/resource/person/index"),
        meta: {
          title: "博奥奖-个人",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "service",
        name: "BoaoService",
        component: () => import("@/views/resource/service/index"),
        meta: {
          title: "博奥奖-服务",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "recent",
        name: "RecentActive",
        component: () => import("@/views/resource/recent/index"),
        meta: {
          title: "近期活动",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "cefecanhui",
        name: "CefeCanhui",
        component: () => import("@/views/resource/cefecanhui/index"),
        meta: {
          title: "CEFE参会",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "cefecanzhan",
        name: "CefeCanzhan",
        component: () => import("@/views/resource/cefecanzhan/index"),
        meta: {
          title: "CEFE参展",
          icon: "marker",
          permissions: ["admin"],
        },
      },
    ],
  },
  {
    path: "/vip",
    component: Layout,
    redirect: "noRedirect",
    name: 'Vip',
    alwaysShow: true,
    meta: { title: '会员管理', icon: 'box-open' },
    children: [
      {
        path: "vipUser",
        name: "VipUser",
        component: () => import("@/views/vip/vipUser/index"),
        meta: {
          title: "会员用户",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "vipApply",
        name: "VipApply",
        component: () => import("@/views/vip/vipApply/index"),
        meta: {
          title: "会员申请",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "payCode",
        name: "PayCode",
        component: () => import("@/views/vip/payCode/index"),
        meta: {
          title: "支付码管理",
          icon: "marker",
          permissions: ["admin"],
        },
      },
      {
        path: "vipUserAsk",
        name: "VipUserAsk",
        component: () => import("@/views/vip/vipUserAsk/index"),
        meta: {
          title: "会员咨询",
          icon: "marker",
          permissions: ["admin"],
        },
      },
    ]
  },
  {
    path: '*',
    redirect: '/404',
    hidden: true,
  },
]

const router = new VueRouter({
  base: publicPath,
  mode: routerMode,
  scrollBehavior: () => ({
    y: 0,
  }),
  routes: constantRoutes,
})

export function resetRouter() {
  location.reload()
}

export default router
