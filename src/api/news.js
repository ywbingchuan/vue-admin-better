import request from '@/utils/request'

// 获取栏目类型列表
export function articleTypeListAll(data) {
  return request({
    url: '/type/listAll',
    method: 'post',
    data,
  })
}

// 修改栏目类型
export function setArticleType(data) {
  return request({
    url: '/type/set',
    method: 'post',
    data,
  })
}

// 添加栏目
export function addArticleType(data) {
  return request({
    url: '/type/add',
    method: 'post',
    data,
  })
}

// 根据ID获取栏目信息
export function getArticleTypeById(data) {
  return request({
    url: '/type/get',
    method: 'post',
    data,
  })
}


// 分页获取资源列表
export function getResourceList(data) {
  return request({
    url: '/upload/getFileListByPage',
    method: 'post',
    data,
  })
}


// 加载某个栏目的变量列表
export function getTypeVarList(data) {
  return request({
    url: '/var/listAll',
    method: 'post',
    data,
  })
}

// 添加某个栏目的变量
export function addTypeVar(data) {
  return request({
    url: '/var/add',
    method: 'post',
    data,
  })
}

// 编辑某个栏目的变量
export function setTypeVar(data) {
  return request({
    url: '/var/set',
    method: 'post',
    data,
  })
}

// 删除某个栏目的变量
export function delTypeVar(data) {
  return request({
    url: '/var/del',
    method: 'post',
    data,
  })
}

// 获取栏目变量类型列表
export function getTypeList() {
  return request({
    url: '/var/typeList',
    method: 'post',
  })
}

//根据栏目id获取该栏目所有文章
export function getArticleListByTypeId(data) {
  return request({
    url: '/article/list',
    method: 'post',
    data,
  })
}

//删除文章
export function delArticleById(data) {
  return request({
    url: '/article/del',
    method: 'post',
    data,
  })
}

// 添加文章
export function addArticleArticle(data) {
  return request({
    url: '/article/add',
    method: 'post',
    data,
  })
}

// 审核文章
export function setArticleIssueTime(data) {
  return request({
    url: '/article/setIssueTime',
    method: 'post',
    data,
  })
}

// 修改文章
export function setArticleArticle(data) {
  return request({
    url: '/article/set',
    method: 'post',
    data,
  })
}

//根据新闻id获取该该新闻信息
export function getArticleById(data) {
  return request({
    url: '/article/get',
    method: 'post',
    data,
  })
}

// 生成网站首页
export function makeIndexHtml(data) {
  return request({
    url: '/make/makeIndexHtml',
    method: 'post',
    data,
  })
}

// 生成列表页以及详细页
export function makeListHtml(data) {
  return request({
    url: '/make/makeListHtml',
    method: 'post',
    data,
  })
}


// 生成详细页
export function makeDetailHtml(data) {
  return request({
    url: '/make/makeDetailHtml',
    method: 'post',
    data,
  })
}

// 生成某个栏目的所有详细页
export function makeDetailHtmlByTypeId(data) {
  return request({
    url: '/make/makeDetailHtmlByTypeId',
    method: 'post',
    data,
  })
}

// 生成所有页面
export function makeAllHtml(data) {
  return request({
    url: '/make/makeAllHtml',
    method: 'post',
    data,
  })
}

// 添加作者分组
export function addAuthorGroup(data) {
  return request({
    url: '/author/addAuthorGroup',
    method: 'post',
    data,
  })
}

// 修改作者分组
export function setAuthorGroup(data) {
  return request({
    url: '/author/setAuthorGroup',
    method: 'post',
    data,
  })
}

// 删除作者分组
export function delAuthorGroupById(data) {
  return request({
    url: '/author/delAuthorGroupById',
    method: 'post',
    data,
  })
}

// 根据ID获取作者分组信息
export function getAuthorGroupById(data) {
  return request({
    url: '/author/getAuthorGroupById',
    method: 'post',
    data,
  })
}

// 获取作者分组信息列表
export function getAuthorGroupList(data) {
  return request({
    url: '/author/getAuthorGroupList',
    method: 'post',
    data,
  })
}

// 添加作者
export function addAuthor(data) {
  return request({
    url: '/author/addAuthor',
    method: 'post',
    data,
  })
}

// 修改作者
export function setAuthor(data) {
  return request({
    url: '/author/setAuthor',
    method: 'post',
    data,
  })
}

// 删除作者
export function delAuthorById(data) {
  return request({
    url: '/author/delAuthorById',
    method: 'post',
    data,
  })
}

// 根据ID获取作者信息
export function getAuthorById(data) {
  return request({
    url: '/author/getAuthorById',
    method: 'post',
    data,
  })
}

// 获取作者信息列表
export function getAuthorList(data) {
  return request({
    url: '/author/getAuthorList',
    method: 'post',
    data,
  })
}

// 分页获取用户
export function getUserByPage(data) {
  return request({
    url: '/user/listByPage',
    method: 'post',
    data,
  })
}


// 分页获取咨询用户
export function getContactUserByPage(data) {
  return request({
    url: '/contact/listByPage',
    method: 'post',
    data,
  })
}

// 分页获取购买用户
export function getBuyUserByPage(data) {
  return request({
    url: '/buy/listByPage',
    method: 'post',
    data,
  })
}

// 增加文件上传资源信息
export function addFileInfoToDb(data) {
  return request({
    url: '/upload/addFileInfo',
    method: 'post',
    data,
  })
}

// 删除文件上传资源信息
export function delFileInfoFromDb(data) {
  return request({
    url: '/upload/delUploadFile',
    method: 'post',
    data,
  })
}

// 添加往届回顾
export function addMeet(data) {
  return request({
    url: '/meet/add',
    method: 'post',
    data,
  })
}

// 删除往届回顾
export function delMeet(data) {
  return request({
    url: '/meet/del',
    method: 'post',
    data,
  })
}

// 修改往届回顾
export function setMeet(data) {
  return request({
    url: '/meet/set',
    method: 'post',
    data,
  })
}

// 根据ID获取往届回顾
export function getMeet(data) {
  return request({
    url: '/meet/get',
    method: 'post',
    data,
  })
}

// 分页获取往届回顾
export function getMeetByPage(data) {
  return request({
    url: '/meet/list',
    method: 'post',
    data,
  })
}


// 添加轮播
export function bannerAddIndex(data) {
  return request({
    url: '/carousel/addIndex',
    method: 'post',
    data,
  })
}

// 修改轮播
export function bannerSetIndex(data) {
  return request({
    url: '/carousel/setIndex',
    method: 'post',
    data,
  })
}

// 删除轮播
export function bannerDelIndex(data) {
  return request({
    url: '/carousel/delIndex',
    method: 'post',
    data,
  })
}

// 获取轮播
export function bannerGetIndex(data) {
  return request({
    url: '/carousel/getIndex',
    method: 'post',
    data,
  })
}

// 分页获取轮播
export function bannerListIndex(data) {
  return request({
    url: '/carousel/listIndex',
    method: 'post',
    data,
  })
}


// 添加轮播
export function bannerAddBody(data) {
  return request({
    url: '/carousel/addBody',
    method: 'post',
    data,
  })
}

// 修改轮播
export function bannerSetBody(data) {
  return request({
    url: '/carousel/setBody',
    method: 'post',
    data,
  })
}

// 删除轮播
export function bannerDelBody(data) {
  return request({
    url: '/carousel/delBody',
    method: 'post',
    data,
  })
}

// 获取轮播
export function bannerGetBody(data) {
  return request({
    url: '/carousel/getBody',
    method: 'post',
    data,
  })
}

// 分页获取轮播
export function bannerListBody(data) {
  return request({
    url: '/carousel/listBody',
    method: 'post',
    data,
  })
}

// 获取报告下载码
export function getDownCodeByUrl(data) {
  return request({
    url: '/upload/getDownCodeByUrl',
    method: 'post',
    data,
  })
}

// 分页获取支付码
export function getPayCodeList(data) {
  return request({
    url: '/paycode/list',
    method: 'post',
    data,
  })
}

// 生成1个支付码
export function addOnePayCode(data) {
  return request({
    url: '/paycode/add',
    method: 'post',
    data,
  })
}


// 获取会员申请列表
export function applyList(data) {
  return request({
    url: '/apply/list',
    method: 'post',
    data,
  })
}

// 管理员审核会员申请
export function applyVerify(data) {
  return request({
    url: '/apply/verify',
    method: 'post',
    data,
  })
}

// 获取用户信息
export function getUserById(data) {
  return request({
    url: '/user/get',
    method: 'post',
    data,
  })
}

// 设置或者取消新闻的会员权限
export function swapNewUser(data) {
  return request({
    url: '/article/setNewMoney',
    method: 'post',
    data,
  })
}

// 获取会员咨询列表
export function getVipAskBypage(data) {
  return request({
    url: '/vipUser/list',
    method: 'post',
    data,
  })
}