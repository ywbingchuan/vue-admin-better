import request from '@/utils/request'
import { encryptedData } from '@/utils/encrypt'
import { loginRSA, tokenName } from '@/config'

export async function adminLogin(data) {
  if (loginRSA) {
    data = await encryptedData(data)
  }
  return request({
    url: '/admin/login',
    method: 'post',
    data,
  })
}

// 分页获取管理员
export function getAdminList(data) {
  return request({
    url: '/admin/list',
    method: 'post',
    data,
  })
}

// 添加管理员
export function addAdmin(data) {
  return request({
    url: '/admin/add',
    method: 'post',
    data,
  })
}

// 修改管理员密码
export function resetPassword(data) {
  return request({
    url: '/admin/resetPassword',
    method: 'post',
    data,
  })
}