import request from '@/utils/request'
import { encryptedData } from '@/utils/encrypt'
import { loginRSA, tokenName } from '@/config'

export async function login(data) {
  if (loginRSA) {
    data = await encryptedData(data)
  }
  return request({
    url: '/login',
    method: 'post',
    data,
  })
}

export function getUserInfo(accessToken) {
  return request({
    url: '/admin/userInfo',
    method: 'post',
    data: {
      [tokenName]: accessToken,
    },
  })
}

export function logout(accessToken) {
  return request({
    url: '/admin/logout',
    method: 'post',
    data: {
      [tokenName]: accessToken,
    },
  })
}

export function register() {
  return request({
    url: '/register',
    method: 'post',
  })
}
