import request from '@/utils/request'

// 获取系统配置列表
export function getSysConfigAll(data) {
  return request({
    url: '/sysconfig/getSysConfigAll',
    method: 'post',
    data,
  })
}

// 获取系统配置
export function setSysConfigById(data) {
  return request({
    url: '/sysconfig/setSysConfigById',
    method: 'post',
    data,
  })
}

// 获取文件上传类型
export function getUploadType() {
  return request({
    url: '/sysconfig/getUploadType',
    method: 'post'
  })
}

// 获取AliyunOSS配置
export function getAliyunOssConfig() {
  return request({
    url: '/sysconfig/getAliyunOssConfig',
    method: 'post'
  })
}

// 设置AliyunOSS配置
export function setAliyunOssConfig(data) {
  return request({
    url: '/sysconfig/setAliyunOssConfig',
    method: 'post',
    data,
  })
}