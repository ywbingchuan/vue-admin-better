
## 安装插件
```bash
# 汉字转拼音
npm install js-pinyin --save

# wangEditor v4
npm i wangeditor --save
# 安装v5版本
npm install @wangeditor/editor-for-vue --save

# aliyun oss
npm install ali-oss --save

# select image
npm i vue-select-image
```

## 栏目维护

- 案例
- 课程
- 文章
- 知名专家
- CEFE
- 大赛活动
- 博奥奖
- 服务商
- 行业报告
- 博奥研究院

## 表单数据类型

```
1 问题咨询
2 合作意向
3 申请参赛
5 博奥奖-企业
6 博奥奖-个人
7 博奥奖-服务
8 近期活动
```


## 参考文章

[wangEditor](https://www.wangeditor.com/)
[Vue解决wangeditor与弹窗互相覆盖问题](https://blog.csdn.net/woshisangsang/article/details/116722971)
[TDK是什么？SEO优化技术之TDK标签，了解一下](https://baijiahao.baidu.com/s?id=1620465625587585893&wfr=spider&for=pc)
[vue打包时在图片处报错，Syntax Error: Error: ‘**\node_modules\pngquant-bin\vendor\pngquant.exe‘](https://blog.csdn.net/lingliu0824/article/details/122365553)
[ElementUI中el-upload上传图片组件上传成功函数传自定义参数](https://blog.csdn.net/qq_46380656/article/details/114574428)
[Thymeleaf中通过strings字符串拆分的灵性操作](https://blog.csdn.net/weixin_49610478/article/details/108980765)
[wangEditor 自定义字号fontSize插件 突破最多7个字号的限制](https://www.jianshu.com/p/ef1c3e77b750)
[wangeditor](https://www.wangeditor.com/v4/)

```
<h1 th:each="user:${list}"  th:text="${user}"></h1>
<h1 th:each="user:${list}" >[[${user}]]</h1>

```

vue-element-admin关闭当前标签页并且返回上一个标签页
```
 // 调用全局挂载的方法,关闭当前标签页
 this.$store.dispatch("tagsView/delView", this.$route);
  
//返回上一个tab页面 返回上一步路由，
 this.$router.go(-1);
//返回到指定路由
 this.$router.push({path: '/reward/hongbao/transfer'})
```

[Vue图片选择(Vue Select Image)](https://madewith.cn/338)

```bash
```

[java视频截图](https://blog.csdn.net/qq_44663816/article/details/120322515)
[阿里云oss视频上传后,如何获取视频封面](https://blog.csdn.net/qq_27295403/article/details/89176708)
[oss获取视频文件第一帧做封面](https://www.cnblogs.com/hllzww/p/13864534.html)
[](https://upload.shenmazong.com/videos/1649166697868_be30ffa9.mp4?x-oss-process=video/snapshot,t_10000,m_fast)

已解决1、文章更新了，时间没有变化
已解决2、文件上传大小
已解决3、编辑完成后没有关闭窗口
4、首页专家，显示6个
5、博奥研究院-服务企业-新增栏目，仅仅显示图片
6、博奥奖-获奖案例（来源干货栏目的案例）
7、所有页面，可加跳转链接
8、详情页-栏目列表链接（面包屑）
9、首页闪屏建议增加按钮
10、文章+专家的关联
11、首页文章标题字符数以及字体大小，需要统一
12、表单页，尽量都是弹出，不要新页面
13、首页近期活动 -》 先以文章发布为主 ： 查看详情
14、课程热榜-》显示课程（现在显示文章）先是置顶：然后是点击率
15、文章标题更新了，但是静态页没有生成（时间）
已解决16、干货banner图片大小一样
17、课程里面的点击定位有点偏移
18、未开播、直播中，都显示的图片：购买显示：预约直播、进入直播、回访隐藏
未直播、直播中、回访、课程
19、弹出窗口、只能提交手机号，提交完了，再继续播放
20、行业报告、专家的分区暂时隐藏
21、演讲嘉宾、服务企业logo、票务信息
22、CEFE点击连到详细页的某个位置
23、评论暂时去掉
24、大赛活动视频：缩小比例
25、大赛活动报名：弹出框
26、大赛案例、博奥奖案例、推荐案例、研究院案例
27、文章和案例，可以没有详情页、也可以指定链接、也可以后台录入内容
28、知名评委：图片
29、参赛企业：图片
30、博奥奖：默认都是收缩、左右对调
31、博奥奖：立即申报、也是页面
32、获奖查询：可以没有详情页、也可以指定链接、也可以后台录入内容
33、服务商：合作对接是表单
34、行业报告、立即下载：都是弹出表单
35、行业报告：PDF预览（放图片）
36、赋能训练营和公开课（同一个栏目）是一个课程的介绍
37、博奥研究院：根据SEO关键字关联
38、登录注册有单独的状态
